# Some python fun for presentations

Source code: [https://bitbucket.org/dpeleshenko/mdshow](https://bitbucket.org/dpeleshenko/mdshow)

Except managing managing my company [Tesseris Pro](http://www.tesseris.com) and working as a developer I am working with student in one of Kharkiv university. That is a good way to find young talents and prepare them to work in our company.

So I am very often preparing some technical presentations. I have a huge archive of presentations form different technologies. When I prepare my lectures I have to merge this presentations update obsolete data and so on. I was always using MS PowerPoint. But reworking presentation by drag'n'drop is terrible. Additionally I have problems with different slide design. So I am completely unsatisfied with existing mouse driven presentation software. 

As a developer I prefer to write everything problem oriented language. First idea was HTML... but it's too complex... too many letters. LaTex, god but too complex too. I have no math symbols and other similar things in my presentation. Much better is [Markdown](https://daringfireball.net/projects/markdown/). After that presentation done with markdown become my dream. And one of important thing was to have every slide in separate file and have separate file for general presentation layout and design. There are some open source solution but none of them was good enough for me. So I decided to make it myself.

My first idea was to convert markdown to HTML and render HTML with [CEF](https://bitbucket.org/chromiumembedded/cef). And taking into account that I have another dream - to develop cross platform desktop applications technology with Python business logic and HTML/CSS/JS presentation. I selected python and CEF as main  technology stack. But unfortunately all CEF binding for python are terrible. I spent whole day trying to run demo code without any success.

After some additional research I have found very good library to statically render HTML/CSS - [http://weasyprint.org/](http://weasyprint.org/) That was completely enough for me. You can find a python script with about 120 lines to show presentations based on directory with markdown files here: [https://bitbucket.org/dpeleshenko/mdshow](https://bitbucket.org/dpeleshenko/mdshow)

To run this code you will need to install following:

- WeasyPrint ( `pip install WeasyPrint` )
- Markdown ( 'pip install markdown' )  
- PyGTK and GTK [http://www.pygtk.org/index.html](http://www.pygtk.org/index.html)

I have tested everything in Ubuntu 15.10 but everything should work in any Linux, Mac or Windows. Maybe some fine-tuning with GTK will be required. 


