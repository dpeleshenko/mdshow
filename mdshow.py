#!/usr/bin/python

import pygtk
pygtk.require('2.0')
import gtk
import markdown
import codecs
import json
import os
from weasyprint import HTML, CSS

class ShowWindow():

    def __init__(self, folder):
        self.index = 0
        self.folder = folder
        self.readSettings()
        self.readFiles()
        self.configureMonitors()
 
        self.showWindow()
        gtk.main()
    
    def show(self, e):
        self.showSlide()
            
    def keyDown(self, widget, event):
        keyCode = event.keyval
        if keyCode == gtk.keysyms.Escape:
            gtk.main_quit()
        elif keyCode == gtk.keysyms.space or keyCode == gtk.keysyms.Down:
            if(self.index < len(self.files)):
                self.index = self.index + 1
                self.showSlide()
        elif keyCode == gtk.keysyms.Up:
            if self.index > 0:
                self.index = self.index - 1
                self.showSlide()
        
    def destroy(self, e):
        gtk.main_quit()

    def showSlide(self):
        if(self.index >= len(self.files)):
            self.image.hide()
        else:
            self.image.show()
            with codecs.open(self.files[self.index], mode="r", encoding="utf-8") as mdFile:
                page = markdown.markdown(mdFile.read())
            
            template = "{content}"
            
            if self.templateFile:
                with codecs.open(self.templateFile, mode="r", encoding="utf-8") as tf:
                    template = tf.read()
            
            page = template.replace("{content}", page).replace("{page}", str(self.index))   
            
            width, height = self.getSize() 
            css = [CSS(string="@page{{width: {0}px; height: {1}px;}}".format(width, height)), CSS(string=self.defaultStyle)]
            if(os.path.exists(self.css)):
                css.append(self.css)
                
            png = HTML(string=page, base_url="file://"+self.folder+"/page.html").write_png(stylesheets=css)
            loader=gtk.gdk.PixbufLoader()
            loader.write(png)
            loader.close()
            self.image.set_from_pixbuf(loader.get_pixbuf())

    def readFiles(self):
        self.files = [os.path.join(self.folder, f) for f in os.listdir(self.folder) 
                      if (os.path.isfile(os.path.join(self.folder, f))
                          and (os.path.join(self.folder, f) != self.templateFile) 
                          and (f.endswith(".md") or f.endswith(".html")))]
        self.files.sort()
            
    def getSize(self):
        #if self.window.
        w, h = self.window.get_size()
        width = h / 3 * 4
        height = h
        if width > w:
            width = w
            height = w / 4 * 3
        return width, height
    
    def configureMonitors(self):
        laptopMonitorName = "eDP1"
        self.screen = gtk.gdk.screen_get_default()
        self.n_monitors = self.screen.get_n_monitors()
        if self.n_monitors > 1:
            monitors = [self.screen.get_monitor_geometry(m) for m in range(self.n_monitors)]
            monitorNames = [self.screen.get_monitor_plug_name(m) for m in range(self.screen.get_n_monitors())]
            if monitorNames.count(laptopMonitorName) > 0:
                helperMonitorIndex = monitorNames.index(laptopMonitorName)
                self.helperMonitor = monitors[helperMonitorIndex]
                self.defaultMonitor = monitors[1 if helperMonitorIndex == 0 else 0]
        else:
            self.helperMonitor = self.screen.get_monitor_geometry(0)
            self.defaultMonitor = self.helperMonitor

    def showWindow(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse('#000000'))
        self.window.connect("key_press_event", self.keyDown)
        self.window.connect("destroy", self.destroy)
        self.window.connect("show", self.show)
        self.image = gtk.Image()
        self.window.add(self.image)
        self.window.set_default_size(self.defaultMonitor.width, self.defaultMonitor.height)
        self.window.move(self.defaultMonitor.x, self.defaultMonitor.y)
        self.window.fullscreen()
        self.window.show_all()
    
    def readSettings(self):
        self.css = os.path.join(self.folder,"style.css")
        self.defaultStyle = "@page{background-color: white; margin:0; padding:0;}"
        self.templateFile = None
        
        settingsFileName = os.path.join(self.folder, "config.mdshow")
        if os.path.exists(settingsFileName):
            with open(settingsFileName) as settingsFile:
                self.settings = json.load(settingsFile)
            
            if self.settings.has_key("css"):
                self.css = os.path.join(self.folder, self.settings["css"])
            if self.settings.has_key("template"):
                self.templateFile = os.path.join(self.folder, self.settings["template"]).encode("utf8") 


ShowWindow(os.path.abspath(os.path.curdir))

